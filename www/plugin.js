function AmazonMobileAssociates() {

	this.init = this.setAppKey = function(key, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'AmazonMobileAssociatesPlugin', 'setAppKey', [key]);
	};

	this.openHomePage = function(successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'AmazonMobileAssociatesPlugin', 'openHomePage', []);
	};

	this.openProductPage = function(asin, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'AmazonMobileAssociatesPlugin', 'openProductPage', [asin]);
	};

	this.openSearchPage = function(searchTerm, category, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'AmazonMobileAssociatesPlugin', 'openSearchPage', [searchTerm, category]);
	};

}

if(typeof module !== undefined && module.exports) {

	module.exports = AmazonMobileAssociates;
}
