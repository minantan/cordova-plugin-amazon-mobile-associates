# Amazon Mobile Associates Cordova Plugin
Add support for Amazon Mobile Associates to your Cordova and Phonegap based mobile apps. Currently only exists for Android. See https://developer.amazon.com/appsandservices/apis/earn/mobile-associates for more info on the mobile associates program.

Largely inspired by https://github.com/blakgeek/cordova-plugin-amazonmobileads

## How do I install it? ##

```
cordova plugin add https://bitbucket.org/minantan/cordova-plugin-amazon-mobile-associates
```

or

```
phonegap local plugin add https://bitbucket.org/minantan/cordova-plugin-amazon-mobile-associates
```

## How do I use it? ##

```javascript
document.addEventListener('deviceready', function() {

	window.ama = new AmazonMobileAssociates();

	// get things started by passing in your app key
	// You'll have to set an app up in the Amazon Developer portal first
	ama.init('<your app key>', function() {
		console.log('AMA initialised!');
	}, function(err) {
		console.error(['oh crap', err]);
	});

	// Open the Amazon home page with your associate tag
	ama.openHomePage(, function() {
		console.log('Homepage opened');
	}, function(err) {
		console.error(['oh crap', err]);
	});

	// Open a product page on Amazon. You will need the product's ASIN
	// (http://en.wikipedia.org/wiki/Amazon_Standard_Identification_Number)
	ama.openProductPage('B0006HCWFI', function() {
		console.log('Product page opened');
	}, function(err) {
		console.error(['oh crap', err]);
	});

	// Open a search page on Amazon. You can specify which department
	ama.openSearchPage('Coloretto', 'Toys & Games', function() {
		console.log('Search page opened');
	}, function(err) {
		console.error(['oh crap', err]);
	});

}, false);
```

