package us.nimbco.cordova.plugin.amazonmobileassociates;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.amazon.device.ads.*;
import com.amazon.device.associates.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

// TODO: add separate methods for loading and displaying interstitial
// TODO: add callback javascript callbacks
// TODO: add support for controlling the ad sizes

public class AmazonMobileAssociatesPlugin extends CordovaPlugin {

    private static final String LOGTAG = "[AmazonMobileAssociatesPlugin]";
    private boolean bannerAtTop = true;
    private AdLayout bannerAdView = null;
    private InterstitialAd interstitialAd = null;
    private ViewGroup blender;
    private ViewGroup webViewContainer;

//    @Override
    protected void pluginInitialize() {
    }

//    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            Log.i(LOGTAG, action);

            if (action.equals("setAppKey")) {
                AssociatesAPI.initialize(new AssociatesAPI.Config(args.getString(0), cordova.getActivity()));

                callbackContext.success();
            } else if (action.equals("openHomePage")) {
                OpenHomePageRequest request = new OpenHomePageRequest();
                try {
                    LinkService linkService = AssociatesAPI.getLinkService();
                    linkService.openRetailPage(request);
                } catch (NotInitializedException e) {
                    Log.e(LOGTAG, "Associates API not initialized", e);
                }
                callbackContext.success();
            } else if (action.equals("openProductPage")) {
                OpenProductPageRequest request = new OpenProductPageRequest(args.getString(0));
                try {
                    LinkService linkService = AssociatesAPI.getLinkService();
                    linkService.openRetailPage(request);
                } catch (NotInitializedException e) {
                    Log.e(LOGTAG, "Associates API not initialized", e);
                }
                callbackContext.success();
            } else if (action.equals("openSearchPage")) {
                final String searchTerm = args.getString(0);
                final String category = args.optString(1);

                OpenSearchPageRequest request = category.length() == 0 ? new OpenSearchPageRequest(searchTerm)
                        : new OpenSearchPageRequest(category, searchTerm);
                try {
                    LinkService linkService = AssociatesAPI.getLinkService();
                    linkService.openRetailPage(request);
                } catch (NotInitializedException e) {
                    Log.e(LOGTAG, "Associates API not initialized", e);
                }
                callbackContext.success();

            } else {

                callbackContext.error("Unknown Action");
                return false;
            }
            return true;
        } catch (JSONException e) {

            Log.e("AmazonMobileAssociatesPlugin", e.getMessage());
            callbackContext.error("AmazonMobileAssociatesPlugin: " + e.getMessage());
            return false;
        }
    }

}
